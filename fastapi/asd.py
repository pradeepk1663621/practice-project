from typing import List

from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.context import CryptContext
from sqlalchemy import create_engine, Column, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from databases import Database



# Create FastAPI instance
app = FastAPI()

# Database configuration

DATABASE_URL = "postgresql://username:password@localhost:5432/ecommerce"  # Replace with your PostgreSQL database details

# Create database connection
database = Database(DATABASE_URL)
metadata = declarative_base()

# Models
class User(metadata):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String)
    is_admin = Column(Boolean, default=False)

class Product(metadata):
    __tablename__ = "products"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    description = Column(String)
    price = Column(Integer)

# Security
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")

# Dependency
def get_db():
    try:
        yield database
    finally:
        database.close()

def get_current_user(token: str = Depends(oauth2_scheme)):
    # Verify token and retrieve user information
    # This implementation assumes the token contains user ID and is_admin flag
    user_id, is_admin = verify_token(token)
    db = get_db()
    user = db.query(User).get(user_id)
    if not user:
        raise HTTPException(status_code=401, detail="Invalid credentials")
    return user

# Routes
@app.post("/register")
def register(user: User, db: Session = Depends(get_db)):
    # Check if username already exists
    existing_user = db.query(User).filter(User.username == user.username).first()
    if existing_user:
        raise HTTPException(status_code=400, detail="Username already exists")

    # Hash the password before saving
    user.password = pwd_context.hash(user.password)

    # Save the new user to the database
    db.add(user)
    db.commit()

    return {"message": "User registered successfully"}

@app.post("/login")
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    # Retrieve user from the database
    user = db.query(User).filter(User.username == form_data.username).first()
    if not user or not pwd_context.verify(form_data.password, user.password):
        raise HTTPException(status_code=400, detail="Invalid username or password")

    # Generate and return the access token
    token = generate_token(user.id, user.is_admin)
    return {"access_token": token, "token_type": "bearer"}

@app.get("/products", response_model=List[Product])
def get_products(user: User = Depends(get_current_user)):
    db = get_db()
    # Regular users can only read products
    if not user.is_admin:
        query = db.query(Product).all()
    else:
        query = db.query(Product).all()
    return query

@app.post("/products")
def create_product(product: Product, user: User = Depends(get_current_user)):
    db = get_db()
    # Only admin users can add products
    if not user.is_admin:
        raise HTTPException(status_code=403, detail="Access denied")

    # Save the new product to the database
    db.add(product)
    db.commit()

    return {"message": "Product created successfully"}

@app.put("/products/{product_id}")
def update_product(product_id: int, updated_product: Product, user: User = Depends(get_current_user)):
    db = get_db()
    # Only admin users can update products
    if not user.is_admin:
        raise HTTPException(status_code=403, detail="Access denied")

    # Retrieve the existing product from the database
    product = db.query(Product).get(product_id)
    if not product:
        raise HTTPException(status_code=404, detail="Product not found")

    # Update the product details
    product.name = updated_product.name
    product.description = updated_product.description
    product.price = updated_product.price
    db.commit()

    return {"message": "Product updated successfully"}

@app.delete("/products/{product_id}")
def delete_product(product_id: int, user: User = Depends(get_current_user)):
    db = get_db()
    # Only admin users can delete products
    if not user.is_admin:
        raise HTTPException(status_code=403, detail="Access denied")

    # Retrieve the product from the database
    product = db.query(Product).get(product_id)
    if not product:
        raise HTTPException(status_code=404, detail="Product not found")

    # Delete the product
    db.delete(product)
    db.commit()

    return {"message": "Product deleted successfully"}

# Main
if __name__ == "__main__":
    metadata.create_all(bind=engine)
    uvicorn.run(app, host="0.0.0.0", port=8000)


#########################
from typing import List, Optional
from sqlalchemy.orm import Session
from .models import User, Product
from .database import SessionLocal


# User CRUD Operations
def create_user(user: User) -> User:
    db = SessionLocal()
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def get_user(username: str) -> Optional[User]:
    db = SessionLocal()
    user = db.query(User).filter(User.username == username).first()
    return user


def get_users() -> List[User]:
    db = SessionLocal()
    users = db.query(User).all()
    return users


def update_user(username: str, user: User) -> Optional[User]:
    db = SessionLocal()
    db_user = db.query(User).filter(User.username == username).first()
    if db_user:
        db_user.username = user.username
        db_user.password = user.password
        db_user.is_admin = user.is_admin
        db.commit()
        db.refresh(db_user)
        return db_user


def delete_user(username: str) -> Optional[User]:
    db = SessionLocal()
    user = db.query(User).filter(User.username == username).first()
    if user:
        db.delete(user)
        db.commit()
        return user


# Product CRUD Operations
def create_product(product: Product) -> Product:
    db = SessionLocal()
    db.add(product)
    db.commit()
    db.refresh(product)
    return product


def get_product(product_id: int) -> Optional[Product]:
    db = SessionLocal()
    product = db.query(Product).filter(Product.id == product_id).first()
    return product


def get_products() -> List[Product]:
    db = SessionLocal()
    products = db.query(Product).all()
    return products


def update_product(product_id: int, product: Product) -> Optional[Product]:
    db = SessionLocal()
    db_product = db.query(Product).filter(Product.id == product_id).first()
    if db_product:
        db_product.name = product.name
        db_product.price = product.price
        db.commit()
        db.refresh(db_product)
        return db_product


def delete_product(product_id: int) -> Optional[Product]:
    db = SessionLocal()
    product = db.query(Product).filter(Product.id == product_id).first()
    if product:
        db.delete(product)
        db.commit()
        return product



#####3

from fastapi import FastAPI, Depends
from .schemas import UserCreateSchema, UserSchema, ProductCreateSchema, ProductSchema
from .crud import (
    create_user,
    get_user,
    get_users,
    create_product,
    get_product,
    get_products,
    update_product,
    delete_product,
)

app = FastAPI()


# User Routes
@app.post("/users", response_model=UserSchema)
async def create_user_route(user: UserCreateSchema) -> UserSchema:
    created_user = await create_user(user)
    return created_user


@app.get("/users/{username}", response_model=UserSchema)
async def get_user_route(username: str) -> UserSchema:
    user = await get_user(username)
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return user


@app.get("/users", response_model=List[UserSchema])
async def get_users_route() -> List[UserSchema]:
    users = await get_users()
    return users


# Product Routes
@app.post("/products", response_model=ProductSchema)
async def create_product_route(product: ProductCreateSchema, user: User = Depends(get_user)) -> ProductSchema:
    if not user.is_admin:
        raise HTTPException(status_code=403, detail="Only admin can create products")
    created_product = await create_product(product)
    return created_product


@app.get("/products/{product_id}", response_model=ProductSchema)
async def get_product_route(product_id: int) -> ProductSchema:
    product = await get_product(product_id)
    if product is None:
        raise HTTPException(status_code=404, detail="Product not found")
    return product


@app.get("/products", response_model=List[ProductSchema])
async def get_products_route() -> List[ProductSchema]:
    products = await get_products()
    return products


@app.put("/products/{product_id}", response_model=ProductSchema)
async def update_product_route(product_id: int, product: ProductCreateSchema, user: User = Depends(get_user)) -> ProductSchema:
    if not user.is_admin:
        raise HTTPException(status_code=403, detail="Only admin can update products")
    updated_product = await update_product(product_id, product)
    if updated_product is None:
        raise HTTPException(status_code=404, detail="Product not found")
    return updated_product


@app.delete("/products/{product_id}", response_model=ProductSchema)
async def delete_product_route(product_id: int, user: User = Depends(get_user)) -> ProductSchema:
    if not user.is_admin:
        raise HTTPException(status_code=403, detail="Only admin can delete products")
    deleted_product = await delete_product(product_id)
    if deleted_product is None:
        raise HTTPException(status_code=404, detail="Product not found")
    return deleted_product



#############

from typing import List
from fastapi import Depends
from .models import User, Product


class UserSchema(BaseModel):
    username: str
    is_admin: bool

    class Config:
        orm_mode = True


class ProductSchema(BaseModel):
    name: str
    price: float


class UserCreateSchema(BaseModel):
    username: str
    password: str
    is_admin: bool


class ProductCreateSchema(BaseModel):
    name: str
    price: float


###########
from pydantic import BaseModel


class User(BaseModel):
    username: str
    password: str
    is_admin: bool


class Product(BaseModel):
    name: str
    price: float
