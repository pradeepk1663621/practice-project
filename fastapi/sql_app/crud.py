#######################

from typing import List, Optional

from fastapi import Depends
from sqlalchemy.orm import Session

from .authentication import hash_password
from .models import User, Product
from sql_app.database import get_db
from .schemas import UserCreateResponse, UserCreateRequest


def add(db, user):
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def get_user_by_email(db, email: str):
    return db.query(User).filter(User.email == email).first()

def read_all_users(db):
    get_users = db.query(User).all()

def get_user_by_user_id(db, user_id: int):
    return db.query(User).filter(User.id == user_id).first()


def create_user(db, user):
    hashed_password = hash_password(password=user.password)
    db_user = User(email=user.email, password=hashed_password, full_name=user.full_name)
    return add(db, db_user)


def create_products(db, product, user_id):
    db_product = Product(name=product.name, price=product.price, quantity=product.quantity, user_id=user_id)
    return add(db, db_product)
