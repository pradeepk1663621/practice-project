import re
from typing import List
from uuid import UUID

from fastapi import FastAPI, HTTPException, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from jose import JWTError, jwt

from sqlalchemy.orm import Session
from sqlalchemy.testing import db

from sql_app.authentication import create_access_token, SECRET_KEY, ALGORITHM, security, verify_password, hash_password
from sql_app.crud import get_user_by_email, create_user, create_products
from sql_app.database import get_db
from sql_app.models import Product, User
from sql_app.schemas import UserCreateRequest, UserCreateResponse, ProductCreateRequest, ProductUpdate, \
    ProductCreateResponse, UserReadResponse

app = FastAPI(title="User Registration")




# API endpoints

from fastapi import HTTPException


@app.post("/register", response_model=UserCreateResponse)
async def register(request: UserCreateRequest, db: Session = Depends(get_db)) -> UserCreateResponse:
    # Email validation
    if not validate_email(request.email):
        raise HTTPException(status_code=400, detail="Invalid email")
    # Password validation
    if not validate_password(request.password):
        raise HTTPException(status_code=400, detail="Invalid password")
    db_user = get_user_by_email(db, email=request.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    db_user = create_user(db, request)
    print(db_user.email, db_user.full_name)
    return db_user
def validate_email(email: str) -> bool:
    # Regular expression pattern for email validation
    pattern = r"^\S+@\S+\.\S+$"
    return re.match(pattern, email) is not None
def validate_password(password: str) -> bool:
    # Regular expression pattern for password validation
    # for minimum characters - {8,}
    # for at least one uppercase english letter - (?=.*?[A-Z])
    # for at least one lowercase english letter - (?=.*?[a-z])
    # for at least one digit - (?=.*?[0-9])
    # for at least one special character - (?=.*?[#?!@$%^&*-])
    pattern = r"^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$"
    return re.match(pattern, password) is not None


# @app.post("/register", response_model=UserCreateResponse)
# async def register(request: UserCreateRequest, db: Session = Depends(get_db)) -> UserCreateResponse:
#     db_user = get_user_by_email(db, email=request.email)
#     if db_user:
#         raise HTTPException(status_code=400, detail="Email already registered")
#     db_user = create_user(db, request)
#     print(db_user.email, db_user.full_name)
#     return db_user


@app.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = get_user_by_email(db, form_data.username)
    # password = hash_password(crea)
    if not user:
        raise HTTPException(status_code=400, detail="Invalid email or password")
    if not verify_password(form_data.password, user.password):
        raise HTTPException(status_code=400, detail="Invalid email or password")
    access_token = create_access_token(data={"sub": user.email})
    return {"access_token": access_token, "token_type": "bearer"}



@app.post("/products", response_model=ProductCreateResponse)
async def create_product(request: ProductCreateRequest, db: Session = Depends(get_db), token: str = Depends(security)) -> ProductCreateResponse:
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: str = payload.get("sub")
        print(email)
        user = get_user_by_email(db, email)


        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        product = create_products(db, product=request, user_id= user.id)
        return product
    except JWTError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token")




@app.get("/get_all_products")
async def read_product(db: Session = Depends(get_db)):
    user_product = db.query(Product).all()
    return user_product



@app.get('/get_all_users', response_model=List[UserCreateResponse])
async def read_users(db: Session = Depends(get_db)):
    get_users = db.query(User).all()
    return get_users


@app.get("/products/{product_id}")
async def read_product(product_id: int, db: Session = Depends(get_db)):
    db_product = db.query(Product).filter(Product.id == product_id).first()
    if not db_product:
        raise HTTPException(status_code=404, detail="Product not found")
    return db_product


@app.get("/read_products_by_user_id/{user_id}")
async def read_products(user_id: UUID, db: Session = Depends(get_db)):
    db_products = db.query(Product).filter(Product.user_id == user_id).all()
    if not db_products:
        raise HTTPException(status_code=404, detail="No products found for the specified user")
    return db_products



@app.put("/products/{product_id}")
async def update_product(product_id: int, updated_product: ProductUpdate,
                         token: str = Depends(security), db: Session = Depends(get_db)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: str = payload.get("sub")
        user = get_user_by_email(db, email)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        db_product = db.query(Product).filter(Product.id == product_id, Product.user_id == user.id).first()
        if not db_product:
            raise HTTPException(status_code=404, detail="Product not found")
        if updated_product.name:
            db_product.name = updated_product.name
        if updated_product.price:
            db_product.price = updated_product.price
        if updated_product.quantity:
            db_product.quantity = updated_product.quantity
        db.commit()
        return db_product
    except JWTError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token")


@app.delete("/products/{product_id}")
async def delete_product(product_id: int, token: str = Depends(security), db: Session = Depends(get_db)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: str = payload.get("sub")
        user = get_user_by_email(db, email)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        db_product = db.query(Product).filter(Product.id == product_id, Product.user_id == user.id).first()
        if not db_product:
            raise HTTPException(status_code=404, detail="Product not found")
        db.delete(db_product)
        db.commit()
        return {"message": "Product deleted successfully"}
    except JWTError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token")
