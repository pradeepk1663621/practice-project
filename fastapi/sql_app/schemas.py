# Pydantic models for request and response

from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class UserCreateRequest(BaseModel):

    email: str
    password: str
    full_name: Optional[str] = None

class UserCreateResponse(BaseModel):
    id: UUID
    email: str
    full_name: str


    class Config():
        orm_mode = True

class UserReadRequest(UserCreateRequest):
    pass

class UserReadResponse(UserCreateResponse):
   pass


    


class UserLogin(BaseModel):
    email: str
    password: str


class ProductCreateRequest(BaseModel):
    name: str
    price: float
    quantity: int

class ProductCreateResponse(BaseModel):
    name: str

    class Config():
        orm_mode = True


class ProductUpdate(BaseModel):
    name: Optional[str] = None
    price: Optional[float] = None
    quantity: Optional[int] = None
